# AWS EBS CSI Driver

This helm chart is for self-managed AWS EBS CSI Driver.

[Official Helm Chart](#https://github.com/kubernetes-sigs/aws-ebs-csi-driver/tree/master/charts/aws-ebs-csi-driver)

[Install Docs](#https://github.com/kubernetes-sigs/aws-ebs-csi-driver/blob/master/docs/install.md)

You can also use the EKS add-on, but I recommend this method instead.


## Deploy

You need an IAM Role in order for this helm chart to work. You can check if it
was created as part of the cluster creation with `eksctl get iamserviceaccount --cluster dev`.
Or you can create it yourself with

```bash
make create-csi-sa CLUSTER=<cluster-name>
```

Create your projects values file, or copy it from another and modify it. All you
need to modify or fill in is the IAM role you created.

Then deploy with

```bash
make deploy PROJECT_VALUES_FILE=<cluster-name>-values.yaml
```


## Calico

If you are using calico, run `kubectl apply -f hostnetwork-patch.yaml` after your
deployment. Or

```bash
make calico
```


