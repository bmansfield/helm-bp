# My Helm Chart

My Helm Chart for a new application


## About

While `helm create <name>` is very convenient. I typically need a little more
right out of the gate when trying to start a new project. And I'm lazy and do
not like repeating myself. Therefore I created this so I can copy and go.


## Usage

How to use this sample helm chart. First copy it over to a new directory.

```bash
git clone
cd helm
cp -r new-helm-example ./myapp
cd myapp
```

Then you will want to find and replace `%NAME%` with whatever you want to call
your app

```bash
sed -i 's/%NAME%/myapp/g' *.*
sed -i 's/%NAME%/myapp/g' templates/*.*
sed -i 's/%NAME%/myapp/g' templates/tests/*.*
```

Then go through at the very least your `values.yaml` and make your changes.
Here are some recommended and common changes most projects will likely change:

* Review the `Chart.yaml` and make sure you are happy with it
* This `README.md`
* The image repository line three
* Your docker registry key if it's different from the one the rest of the helm
  charts are using
* select your deploymentType on line 22 in the `values.yaml`
* vault annotation for your secrets if you are even using vault. Line 48
* your applications start command. Line 60
* your applications environment variables. Line 66
* your applications ingress setup with tls. Line 101

Those are the major ones. But most the settings should get something very very
basic running. Though take your time and go through each setting and make sure
it's as close to what you think it needs in order to run.

Past the basic `values.yaml` we've provided you with the `development.yaml`
helm values file as well. We recommend using this especially so you separate
your development settings from your production values, once your project graduates
to production.


## Deploy

```bash
make deploy
```


## Features

Added optional deployment type `StatefulSet`. Added `namespace` option.
Configurable environment variables. I'm sure there are some more, but just some
highlights.

Vault ready to go

Cert Manager ready to go

Argo CD ready to go


## Future Improvements

This is just the start. There are many things I would like to see added. Better
configurability of liveness, readiness, and startup probes. Arguments, command,
mounted volumes, init containers, and I'm sure many more.


## Extras

I added some extra multi-tool and dnsutil pods you can ad-hoc deploy that were
very helpful in troubleshooting for me. Just `kubectl apply -f <filename>.yaml`
them.

