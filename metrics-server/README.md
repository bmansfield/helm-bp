# Kubernetes Metrics Server

[Kubernetes Metrics Server](https://github.com/kubernetes-sigs/metrics-server)

Metrics Server collects resource metrics from Kubelets and exposes them in
Kubernetes apiserver through Metrics API for use by Horizontal Pod Autoscaler
and Vertical Pod Autoscaler.


## About

You should only need to deploy this once on a new cluster, and more or less
forget about it.

You should not need a `values.yaml` file for this. You can refer to the official
Helm Chart and review the `values.yaml` and see if there is configurations you
might want to change, then add it in, and deploy over the existing one.

