# AWS Load Balancer Controller

[Official AWS Load Balancer Controller Helm Chart](https://github.com/kubernetes-sigs/aws-load-balancer-controller/tree/main/helm/aws-load-balancer-controller)


## Deploy

You need to add an IAM policy and install the CRD's first

```bash
make iam
```

and

```bash
make crds
```

Then you can deploy the LB Controller. Make sure to update your `values.yaml`
with appropriate values first.

```bash
make deploy
```

