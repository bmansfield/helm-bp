#!/bin/bash

#
# Creates the iam policy and role for dns solver feature
#


CLUSTER_NAME=${CLUSTER_NAME:=$1}


set -e


# Create the IAM Policy
POLICY_ARN=$(aws iam \
                  create-policy \
                  --policy-name "cert-manager-dns-$CLUSTER_NAME" \
                  --policy-document file://dns-policy.json | \
                  jq -r '.Policy.Arn')


# Create the IAM Role
aws iam \
    create-role \
    --role-name "cert-manager-$CLUSTER_NAME" \
    --assume-role-policy-document file://trust-policy.json


# Attach the IAM policy to the roll
aws iam \
    attach-role-policy \
    --role-name "cert-manager-$CLUSTER_NAME" \
    --policy-arn "${POLICY_ARN}"

