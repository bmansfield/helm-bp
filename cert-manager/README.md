# Certificate Manager

[https://cert-manager.io/](https://cert-manager.io/)

[Official Helm Chart](https://github.com/cert-manager/cert-manager/tree/master/deploy/charts/cert-manager)

Cloud native x.509 Kubernetes certificate manager.

Automate your kubernetes certificate management.


## About

Certificate Manager to manage TLS Certificates


## How To Deploy

Make sure you have updated your `trust-policy.json` according to your cluster.
You can find out the relative information from running

```bash
aws eks describe-cluster --name <cluster-name> --query "cluster.identity.oidc.issuer" --output text
```

Then fill in the; region, account, oidc issuer id, and role name to match your
to be unique to your cluster only. i.e. `system:serviceaccount:cert-manager:cert-manager-<cluster-name>`.
You also need to update the `values.yaml` `serviceAccount.name` to match this.
Then fill in the annotations to match this role.

Now we can run

```bash
make iam CLUSTER_NAME=<cluster-name>
```

This is all the prep work to make sure we can actually deploy with

```bash
make deploy
```

Immediately following deploying it. Once cert-manager pods are running fine, you
will need to deploy a `Issuer` or `ClusterIssuer`. It's what is really is deploying
the `Certificates` when you add the cert-manager annotation to your `Ingress`.

Copy an existing one and modify the hosted zone and dnszone values. It is best to
test this by deploying anything which uses this Issuer. Typically you can get hung
on some hiccups with the acme validator.

An example would be

```bash
cp <old-cluster-name>-clusterissuer.yaml <new-cluster-name>-clusterissuer.yaml
```

Then modify the hosted zone id, region and the hosted zone base url. Once you have
done that, run

```bash
kubectl apply -f <new-cluster-name>-clusterissuer.yaml
```

And now you are ready to use it.


## Using Cert-Manager

In order to use `cert-manager` to issue and manage your `Ingress` TLS certificates
you will want to create an `Ingress` with the following

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    cert-manager.io/cluster-issuer: letsencrypt # or whatever you named your clusterIssuer
  labels:
    ...
  name: my-app
  namespace: some-namespace
spec:
  ingressClassName: nginx
  rules:
  - host: my-app.domains.com
    http:
      paths:
      - backend:
          service:
            name: some-service
            port:
              number: 8080
        path: /
        pathType: Prefix
  tls:
  - hosts:
    - my-app.domain.com
    secretName: my-tls-letsencrypt # whatever you want to name your tls secret
```

And that's it.


