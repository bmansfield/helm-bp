NAME ?= cert-manager
NAMESPACE ?= cert-manager
CLUSTER_NAME ?=
VALUES_FILE ?= values.yaml
ENVIRONMENT_VALUES_FILE ?=
CHART_REPO ?= jetstack
CHART_NAME ?= cert-manager
CHART ?= $(CHART_REPO)/$(CHART_NAME)
CHART_VERSION ?= v1.13.1

all: install

default: install

repo init:
	helm repo add $(CHART_REPO) https://charts.jetstack.io
	helm repo update

iam:
	bash create-role.sh $(CLUSTER_NAME)

upgrade update deploy install:
ifeq ($(ENVIRONMENT_VALUES_FILE),)
	helm upgrade \
		-i $(NAME) \
		-f $(VALUES_FILE) \
		-n $(NAMESPACE) \
		--create-namespace \
		--version $(CHART_VERSION) \
		$(CHART)
else
	helm upgrade \
		-i $(NAME) \
		-f $(VALUES_FILE) \
		-f $(ENVIRONMENT_VALUES_FILE) \
		-n $(NAMESPACE) \
		--create-namespace \
		--version $(CHART_VERSION) \
		$(CHART)
endif

mk-upgrade mk-update mk-deploy mk-install:
	helm upgrade \
		-i $(NAME) \
		-n $(NAMESPACE) \
		-f minikube.yaml \
		--create-namespace \
		--version $(CHART_VERSION) \
		$(CHART)

delete del rm clean:
	helm uninstall $(NAME) -n $(NAMESPACE)

ls list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

.PHONY: init repo iam install upgrade update deploy delete del rm clean mk-upgrade mk-update mk-deploy mk-install ls list
