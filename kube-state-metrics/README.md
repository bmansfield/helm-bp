# Kubernetes State Metrics

[Kubernetes State Metrics](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-state-metrics)

kube-state-metrics (KSM) is a simple service that listens to the Kubernetes API
server and generates metrics about the state of the objects. (See examples in
the Metrics section below.) It is not focused on the health of the individual
Kubernetes components, but rather on the health of the various objects inside,
such as deployments, nodes and pods.

kube-state-metrics is about generating metrics from Kubernetes API objects without
modification. This ensures that features provided by kube-state-metrics have the
same grade of stability as the Kubernetes API objects themselves. In turn, this
means that kube-state-metrics in certain situations may not show the exact same
values as kubectl, as kubectl applies certain heuristics to display comprehensible
messages. kube-state-metrics exposes raw data unmodified from the Kubernetes API,
this way users have all the data they require and perform heuristics as they see
fit.

The metrics are exported on the HTTP endpoint /metrics on the listening port
(default 8080). They are served as plaintext. They are designed to be consumed
either by Prometheus itself or by a scraper that is compatible with scraping a
Prometheus client endpoint. You can also open /metrics in a browser to see the
raw metrics. Note that the metrics exposed on the /metrics endpoint reflect the
current state of the Kubernetes cluster. When Kubernetes objects are deleted they
are no longer visible on the /metrics endpoint.


## About

You should only need to deploy this once on a new cluster, and more or less
forget about it.

You should not need a `values.yaml` file for this. You can refer to the official
Helm Chart and review the `values.yaml` and see if there is configurations you
might want to change, then add it in, and deploy over the existing one.


## Deploy

You should have already deployed the `grafana-agent-operator` helm chart. If you
have not done so, then please go back and do that first.

Deploying the `kube-state-metrics` helm chart is rather straight forward. If you
want to use Grafana's "Kubernetes Observability/Monitoring" feature, then you
are all set with the `values.yaml` file.

```bash
make init
make deploy
make integrations
```


