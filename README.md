# Helm Boilerplate

This is my personal Helm Boilerplate repository where I keep my own version of
common third party helm values. So that I can quickly just copy over the
`values.yaml` and make some slight changes and get the chart up and running
quicker.


## Helpful Tips

When you are working on a chart that might have newer version, you should probably
do a quick check of which versions are available, and update your `CHART_VERSION`
in the `Makefile`

```bash
# get the latest from the remote repository
helm repo update
# list them out
helm search repo -l repo/chart
```

For most of these charts, if you are working on a machine which probably hasn't
installed any of these charts before, you probably want to run the `helm repo add repo`
command. Most of these I have a convenient `make repo` or `make init` target for
this step.

Also for many of these, they require other initializations like; creating a `TLS`
or maybe an `aws iam` step, or fetching some `CRD`'s. So be sure to check on those.


## Suggested AWS Kick Start Charts

If you are starting a brand new cluster in AWS, 99% of the time you are going to
want the following in your cluster:

* cluster-autoscaler
* external-dns
* metric-server
* kube-state-metrics
* ingress-nginx
* cert-manager
* jenkins
* vault
* grafana-agent
* velero

Now you should be ready to start your new project kubernetes style


## Future Additions

Helm Charts I would like to add

* prometheus
* elk-stack
* kubecost
* sonarqube
* openvpn
* postgres

