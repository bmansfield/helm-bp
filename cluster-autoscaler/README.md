# Kubernetes Cluster Autoscaler

[Official Kubernetes Cluster Autoscaler Helm Chart](https://github.com/kubernetes/autoscaler/tree/master/charts/cluster-autoscaler)

My version of cluster-autoscaler helm chart in order to get me kick started for AWS.


## IAM

Don't forget to setup your iam permission


## Values

Some popular values you may want to change would be

```yaml
autoscalingGroups:
  - name: asg1
    maxSize: 2
    minSize: 1
```


## Deploy

```bash
make deploy
```

