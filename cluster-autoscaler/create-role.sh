#!/bin/bash

#
# Creates the iam policy and role for cluster-autoscaler
#


CLUSTER_NAME=${CLUSTER_NAME:=$1}
AWS_ACCT=${AWS_ACCT:=$2}


set -e


# Create the IAM Policy
POLICY_ARN=$(aws iam \
                  create-policy \
                  --policy-name "cluster-autoscaler-$CLUSTER_NAME" \
                  --policy-document file://autoscaler-policy.json | \
                  jq -r '.Policy.Arn')

# Create the IAM Role
aws iam \
    create-role \
    --role-name "cluster-autoscaler-$CLUSTER_NAME" \
    --assume-role-policy-document file://trust-policy.json


# Attach the IAM policy to the roll
aws iam \
    attach-role-policy \
    --role-name "cluster-autoscaler-$CLUSTER_NAME" \
    --policy-arn "$POLICY_ARN"

