# Loki

[Official Loki Distributed Helm Chart](https://github.com/grafana/helm-charts/tree/main/charts/loki-distributed)

Loki is a log aggregation system designed to store and query logs from all your
applications and infrastructure.

User the `Makefile` to deploy

```bash
make init
make deploy
```

## LogsInstance

Once the helm chart is deployed and up and running. You should also have the
`grafana-agent-operator` deployed. You will need to deploy the logging CRD's.

[Official Documentation](https://grafana.com/docs/agent/latest/operator/deploy-agent-operator-resources/#deploy-logsinstance-and-podlogs-resources)

You will want to deploy your `LogsInstance` and your `PodLogs` in order for you
to actually start pushing logs to loki and thus into Grafana if you have your
loki datasource setup correctly.

Example grafana helm chart datasource

```yaml
datasources:
  datasources.yaml:
    apiVersion: 1
    datasources:
    - name: Mimir
      type: prometheus
      uid: mimir
      url: http://mimir-nginx.monitoring.svc/prometheus/
      access: proxy
      isDefault: true
      orgId: 1
    - name: Loki
      type: loki
      uid: loki
      url: http://loki-loki-distributed-gateway.monitoring.svc.cluster.local
      access: proxy
      orgId: 1
```

Now deploy your `LogsInstance` with

```bash
make loki
```

There is an example `PodLogs` in the repo `app-podlogs.yaml`. Replace the obvious
fields in there. Looks at the linked documentation above if you need help.
Uncomment the second `kubectl apply -f` line in the `make loki` target, and run
it again. If you followed the documentation correctly, and everything else is set
up, then you should be scraping your applications pods logs and shipping them to
the `LogsInstance` which should have a url pointing to your Loki's gateway kubernetes
service within your cluster.

You should now be able to go to your grafana instances and Explore but checking
for any number of kubernetes labels such as `namespace` and `pod` and select the
pod you are scraping.

