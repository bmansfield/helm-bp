# Grafana

[Official Grafana Helm Chart](https://github.com/grafana/helm-charts/tree/main/charts/grafana)

The open and composable observability and data visualization platform. Visualize
metrics, logs, and traces from multiple sources like Prometheus, Loki,
Elasticsearch, InfluxDB, Postgres and many more.

User the `Makefile` to deploy

```bash
make init
make deploy
```

