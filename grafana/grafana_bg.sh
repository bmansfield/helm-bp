#!/bin/bash

PORT=80

kubectl port-forward -n monitoring svc/grafana 8080:$PORT &

while true ; do nc -vz 127.0.0.1 $PORT; sleep 10 ; done
