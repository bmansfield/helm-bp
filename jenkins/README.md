# Jenkins

[Official Helm Chart for Jenkins](https://github.com/jenkinsci/helm-charts/tree/main/charts/jenkins).

It comes kubernetes ready with the [Kubernetes Plugin](https://plugins.jenkins.io/kubernetes/)
so that it can spawn agents in pods.


## Preparation

You will need three secrets deployed before you can deploy your Jenkins

* docker repository config
* admin password
* aws key and secret


## Backup

It comes with a scheduled backup cron job which saves the snapshots in S3. You
do need to have a secret ready with the credentials.

