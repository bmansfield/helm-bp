#!/bin/bash

#
# Creates jenkins iam role and attach ebs policy to it
#



set -e


CLUSTER_NAME=${CLUSTER_NAME:=$1}
POLICY_ARN="arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"


# Create the IAM Role
aws iam \
    create-role \
    --role-name "jenkins-role-$CLUSTER_NAME" \
    --assume-role-policy-document file://trust-policy.json


# Attach the IAM policy to the role
aws iam \
    attach-role-policy \
    --role-name "jenkins-role-$CLUSTER_NAME" \
    --policy-arn "$POLICY_ARN"

