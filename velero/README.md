# Velero

[Velero Persistent Volume Snapshotter](https://github.com/vmware-tanzu/helm-charts/tree/main/charts/velero)

Velero is an open source tool to safely backup and restore, perform disaster
recovery, and migrate Kubernetes cluster resources and persistent volumes.

Velero has two main components: a CLI, and a server-side Kubernetes deployment.


## Deployment

Also very much like the rest of the Helm Chart deployments. Use the `make` command
or your native `helm` command. You should only need to set this up once.

After it's setup, you can connect to it from your local, and tune the adjustments
of where it snapshots, when, how often, etc. See their [official documentation](https://velero.io/docs/v1.8/customize-installation/)
for more details of how to customize your snapshotting strategy.

