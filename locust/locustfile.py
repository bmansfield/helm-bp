#!/usr/bin/env python3

"""
Load Testing With Locust 2.32.1
"""

import os
import time
from locust import FastHttpUser, TaskSet, task, events, tag, between

from flask import Blueprint, redirect, request, session, url_for
from flask_login import UserMixin, login_user


"""
Load Tests
"""
class CallAnotherEndPoint(TaskSet):
    @tag('tag1')
    @task(1)
    def view_more_items(self):
        for item_id in range(10):
            self.client.get(f"/more_items?id={item_id}", name="/more_items")
            time.sleep(1)

class CallEndPoint(TaskSet):

    def on_start(self):
        self.client.post("/login", json={"username":"foo", "password":"bar"})

    @tag('tag1')
    @task(1)
    def view_items(self):
        for item_id in range(10):
            self.client.get(f"/item?id={item_id}", name="/item")
            time.sleep(1)

    @tag('tag2')
    @task(2)
    def testEndPoint(self):
        with self.client.get("/testingEndPoint", headers={'Content-Type': 'application/json', 'Accept': 'application/json'}, catch_response=True) as response:
            if response.text != "Success":
                response.failure("Got wrong response")
            elif response.elapsed.total_seconds() > 0.5:
                response.failure("Request took too long")
            elif response.status_code == 422:
                    response.success()

class ApiUser(FastHttpUser):
    tasks = [CallEndPoint]
    wait_time = between(1, 3)

class AnotherApiUser(FastHttpUser):
    tasks = [CallAnotherEndPoint]
    wait_time = between(1, 3)


"""
Extending the UI with Web UI Login
"""
class AuthUser(UserMixin):
    def __init__(self, username):
        self.username = username

    def get_id(self):
        return self.username

def load_user(username):
    return AuthUser(username)

@events.init.add_listener
def locust_init(environment, **_kwargs):
    if environment.web_ui:
        auth_blueprint = Blueprint("auth", "web_ui_auth", url_prefix=environment.parsed_options.web_base_path)

        environment.web_ui.login_manager.user_loader(load_user)
        environment.web_ui.app.config['SECRET_KEY'] = os.getenv('LOCUST_PASS')
        base_url = os.getenv('BASE_URL')

        environment.web_ui.auth_args = {
            'username_password_callback': f'{base_url}/login_submit'
        }

        @auth_blueprint.route("/login_submit", methods=["POST"])
        def login_submit():
            username = request.form.get('username')
            password = request.form.get('password')

            if password:
                login_user(AuthUser(username))
                return redirect(url_for('locust.index'))

            session['auth_error'] = 'Invalid username or password'
            return redirect(url_for('locust.login'))

        environment.web_ui.app.register_blueprint(auth_blueprint)

