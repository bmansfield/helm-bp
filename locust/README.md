# Locust

[Locust](https://locust.io/) from [Delivery Hero's Helm Chart](https://github.com/deliveryhero/helm-charts/tree/master/stable/locust).

Locust Helm Chart


## How To Use

1. Write your `locustfile.py`
2. Create a `ConfigMap` out of your `locastfile.py` with it `make cm`
3. Then deploy `make deploy`


## Web UI Authentication

If you are using the built in Locust Web UI Authentication then you will need to
enable it in the `values.yaml`.

And you will need to deploy a secret. Then refer to it in your `loadtest.environment_external_secret`


## Local Testing

To do local testing with your `locustfile.py` to make sure it works before
deploying a broken one to helm.

```bash
docker run -p 8089:8089 --mount type=bind,source="$(pwd)",target=/mnt/locust -e LOCUST_PASS="abc123" -e BASE_URL="http://localhost:8089" locustio/locust:2.32.2 -f /mnt/locust/locustfile.py --web-login --web-base-path '/' --class-picker
```

