path "secrets/staging/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}

path "kv/staging/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}
