#/bin/bash

#
# Setting up PKI for vault
#


ROOT_COMMON_NAME="sub.acme.com"
ROOT_CA_DESC="ACME PKI Root CA"
ROOT_CA_COUNTRY="USA"
ROOT_CA_STATE="CA"
ROOT_CA_ORG="ACME Org"
ROOT_CA_DEPT="IT,Devops"
ROOT_CA_TTL="105120h"     # 12 years
ROOT_CERT_KEY_BITS="4096"



# init root CA PKI
ROOT_MAX_TTL=$(echo $ROOT_CA_TTL | sed 's/[^0-9]//g' | xargs -n 1 bash -c 'echo $(($0+2))h')
vault secrets \
      enable \
      -path=pki_root_ca \
      -description="$ROOT_CA_DESC" \
      -max-lease-ttl="$ROOT_MAX_TTL" \
      pki

# generate root certificate
ROOT_CERT_TTL=$(echo $ROOT_CA_TTL | sed 's/[^0-9]//g' | xargs -n 1 bash -c 'echo $(($0+1))h')
vault write \
      -format=json \
      pki_root_ca/root/generate/internal \
      common_name="$ROOT_COMMON_NAME" \
      country="$ROOT_CA_COUNTRY" \
      locality="$ROOT_CA_STATE" \
      organization="$ROOT_CA_ORG" \
      ou="$ROOT_CA_DEPT" \
      ttl="$ROOT_CERT_TTL" > root_ca.json

# get pem format
# cat root_ca.json | jq -r '.data.key' > acme-pki-ca.key.pem
cat root_ca.json | jq -r '.data.certificate' > acme-pki-ca.cert.pem

# configure CA and CRL URLs
vault write \
      pki_root_ca/config/urls \
      issuing_certificates="https://127.0.0.1:8200/v1/pki_root_ca/ca" \
      crl_distribution_points="https://127.0.0.1:8200/v1/pki_root_ca/crl"

