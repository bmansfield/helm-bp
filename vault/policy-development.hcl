path "secrets/development/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}
