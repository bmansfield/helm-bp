#!/bin/bash


set -e


POLICY_NAMES=(admin devops development staging production jenkins default)


for policy_name in "${POLICY_NAMES[@]}"; do
  TOKEN=$(vault token create -format=json -policy="$policy_name" | jq -r ".auth.client_token")
  echo "Token for role $policy_name is $TOKEN"
done
