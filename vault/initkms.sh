#!/bin/bash

#
# Creates the iam policy, role, kms, and k8s secret for autounseal feature
#


set -e


APP_NAME=${APP_NAME}
APP_DESC=${APP_DESC}
ENVIRONMENT=${ENVIRONMENT:=dev}
NAMESPACE=${NAMESPACE:=vault}


# Create the IAM Policy
POLICY_ARN=$(aws iam \
                  create-policy \
                  --policy-name ${APP_NAME}-${ENVIRONMENT}-VaultKMSUnsealPolicy \
                  --policy-document file://kms-policy.json | \
                  jq -r '.Policy.Arn')


# Create the IAM Role
aws iam \
    create-role \
    --role-name ${APP_NAME}-${ENVIRONMENT}-VaultKMSUnsealRole \
    --assume-role-policy-document file://vault-trust-policy.json


# Attach the IAM policy to the roll
aws iam \
    attach-role-policy \
    --role-name ${APP_NAME}-${ENVIRONMENT}-VaultKMSUnsealRole \
    --policy-arn "${POLICY_ARN}"


# Create the KMS Key
KEY_ID=$(aws kms \
              create-key \
              --description "${APP_DESC} ${ENVIRONMENT} Unseal Vault" \
              --tags TagKey=Purpose,TagValue=VaultUnseal | \
              jq -r '.KeyMetadata.KeyId')


# Create the KMS Alias
aws kms \
    create-alias \
    --alias-name alias/${APP_NAME}-${ENVIRONMENT}-vault-unseal-key \
    --target-key-id "${KEY_ID}"


# Create the Kubernetes Secret
kubectl create namespace ${NAMESPACE} || true
kubectl create \
        secret \
        generic \
        vault-kms-id \
        -n ${NAMESPACE} \
        --from-literal=VAULT_UNSEAL_KMS_KEY_ID="${KEY_ID}"

