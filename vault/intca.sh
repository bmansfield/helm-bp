#/bin/bash

#
# Setting up intermediate CA
#


INT_COMMON_NAME="sub.acme.com"
INT_CA_DESC="PKI Intermediate CA"
INT_CA_COUNTRY="USA"
INT_CA_STATE="CA"
INT_CA_ORG="ACME Org"
INT_CA_DEPT="IT,Devops"
INT_CA_TTL="52560h"      # 6 years
INT_CERT_KEY_BITS="4096"
ROLE_NAME="acme-org"
REQUEST_CERT_TTL="24h"


# init intermediate CA PKI
INT_MAX_TTL=$(echo $INT_CA_TTL | sed 's/[^0-9]//g' | xargs -n 1 bash -c 'echo $(($0+2))h')
vault secrets \
      enable \
      -path=pki_int_ca \
      -description="$INT_CA_DESC" \
      -max-lease-ttl="$INT_MAX_TTL" \
      pki

# generate intermediate CSR
INT_CERT_TTL=$(echo $INT_CA_TTL | sed 's/[^0-9]//g' | xargs -n 1 bash -c 'echo $(($0+1))h')
vault write \
      -format=json \
      pki_int_ca/intermediate/generate/internal \
      common_name="$INT_COMMON_NAME" \
      country="$INT_CA_COUNTRY" \
      locality="$INT_CA_STATE" \
      organization="$INT_CA_ORG" \
      ou="$INT_CA_DEPT" \
      ttl="$INT_CERT_TTL" | jq -r '.data.csr' > acme-pki-overlord.csr.pem

# sign intermediate CSR and generate certificate
vault write \
      -format=json \
      pki_root_ca/root/sign-intermediate \
      csr=@acme-pki-overlord.csr.pem \
      country="$INT_CA_COUNTRY" \
      locality="$INT_CA_STATE" \
      organization="$INT_CA_ORG" \
      ou="$INT_CA_DEPT" \
      format=pem_bundle \
      ttl="$INT_CA_TTL" > intermediate_ca.json

# get pem format
cat intermediate_ca.json | jq -r '.data.key' > acme-pki-overlord.key.pem
cat intermediate_ca.json | jq -r '.data.certificate' > acme-pki-overlord.cert.pem
cat intermediate_ca.json | jq -r '.data.csr' > acme-pki-overlord.csr.pem

# make certificate chain bundle
cat acme-pki-ca.cert.pem > acme-pki-ca-overlord-chain.cert.pem
cat acme-pki-overlord.cert.pem >> acme-pki-ca-overlord-chain.cert.pem

# import signed certificate
vault write \
      -format=json \
      pki_int_ca/intermediate/set-signed \
      certificate=@acme-pki-overlord.cert.pem \
      format=pem_bundle \
      ttl="$TTL"

# configure intermediate CA and CRL URLs
vault write \
      pki_int_ca/config/urls \
      issuing_certificates="https://127.0.0.1:8200/v1/pki_int_ca/ca" \
      crl_distribution_points="https://127.0.0.1:8200/v1/pki_int_ca/crl"

# create a role
vault write \
      pki_int_ca/roles/$ROLE_NAME \
      allowed_domains="$INT_COMMON_NAME" \
      allow_subdomains=true \
      max_ttl="$INT_CA_TTL" \
      ttl="$INT_CA_TTL" \
      allow_ip_sans=true \
      allowed_uri_sans="*.$INT_COMMON_NAME" \
      key_type="rsa" \
      key_bits="$INT_CERT_KEY_BITS" \
      client_flag=false \
      server_flag=true \
      code_signing_flag=false \
      email_protection_flag=false

# issue the int certificate
vault write \
      pki_int_ca/issue/$ROLE_NAME \
      common_name="rnd.$INT_COMMON_NAME" \
      ttl="$REQUEST_CERT_TTL" \
      ip_sans="127.0.0.1" \
      uri_sans="vault.$INT_COMMON_NAME"

