NAME ?= vault
NAMESPACE ?= vault
VALUES_FILE ?= values.yaml
CHART_REPO ?= hashicorp
CHART_NAME ?= vault
CHART ?= $(CHART_REPO)/$(CHART_NAME)
CHART_VERSION ?= 0.27.0
APP_NAME ?=
APP_DESC ?=
ENVIRONMENT ?=
VAULT_APP_NAME ?=

all: install

default: install

repo init:
	helm repo add hashicorp https://helm.releases.hashicorp.com
	helm repo update

certs:
	bash ./genkeys.sh

initkms:
	APP_NAME=$(APP_NAME) \
		APP_DESC="$(APP_DESC)" \
		ENVIRONMENT=$(ENVIRONMENT) \
		./initkms.sh

initvault:
	vault secrets enable -path=secrets kv-v2
	vault auth enable kubernetes
	vault auth enable approle
	bash ./apply-policies.sh
	bash ./create-policy-token.sh
	bash ./apply-roles.sh
	vault write secrets/config max_versions=4
	vault write secrets/config cas_required=true
	vault read secrets/config	# print out config just view convenience

rootca:
	bash rootca.sh

intca:
	bash intca.sh

issuer:
	bash issuer.sh

# ad-hoc initialize app for use with vault from argument passed
initvaultapp:
	vault policy write $(VAULT_APP_NAME) ./vault-$(VAULT_APP_NAME)-policy.hcl
	$(eval TOKEN ?= $(shell vault token create -format=json -policy=$(VAULT_APP_NAME) | jq -r ".auth.client_token"))
	@echo Token for role $(VAULT_APP_NAME) is ${TOKEN}
	vault write \
	      auth/kubernetes/role/$(VAULT_APP_NAME) \
	      bound_service_account_names=vault-agent-injector \
	      bound_service_account_names=vault-auth \
	      bound_service_account_names=$(VAULT_APP_NAME) \
	      bound_service_account_namespaces=vault \
	      bound_service_account_namespaces=$(VAULT_APP_NAME) \
	      policies=$(VAULT_APP_NAME) \
	      ttl=24h
	vault write \
	      auth/approle/role/$(VAULT_APP_NAME) \
	      bound_service_account_names=vault-agent-injector \
	      bound_service_account_names=vault-auth \
	      bound_service_account_names=$(VAULT_APP_NAME) \
	      bound_service_account_namespaces=vault \
	      bound_service_account_namespaces=$(VAULT_APP_NAME) \
	      policies=$(VAULT_APP_NAME) \
	      ttl=24h

upgrade update deploy install:
	helm upgrade \
		-i $(NAME) \
		-n $(NAMESPACE) \
		-f values.yaml \
		--set injector.caBundle=$(shell cat ./cert/vault.ca | base64 | tr -d '\n') \
		--create-namespace \
		--version $(CHART_VERSION) \
		$(CHART)

mk minikube:
	helm upgrade \
		-i $(NAME) \
		-n $(NAMESPACE) \
		-f minikube.yaml \
		--create-namespace \
		--version $(CHART_VERSION) \
		$(CHART)

delete del rm:
	helm uninstall $(NAME) -n $(NAMESPACE)

ls list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

.PHONY: repo init certs initkms initvault rootca intca issuer initvaultapp upgrade update deploy install mk minikube delete del rm ls list
