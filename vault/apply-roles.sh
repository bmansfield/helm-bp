#!/bin/sh


#
# Script to help provision new vault instances in kubernetes with roles
#


set -e


export CLUSTER_ADDR="$(kubectl config view -o jsonpath="{.clusters[0].cluster.server}")"


# tell vault how to authenticate with kubernetes
vault write \
      auth/kubernetes/config \
      token_reviewer_jwt="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" \
      kubernetes_host=${CLUSTER_ADDR} \
      kubernetes_ca_cert=@/var/run/secrets/kubernetes.io/serviceaccount/ca.crt


#
# Roles
#

# admin
vault write \
      auth/kubernetes/role/admin \
      bound_service_account_names=vault-agent-injector \
      bound_service_account_names=vault-auth \
      bound_service_account_names=app \
      bound_service_account_namespaces=default \
      policies=admin \
      ttl=24h

# devops
vault write \
      auth/kubernetes/role/devops \
      bound_service_account_names=vault-agent-injector \
      bound_service_account_names=vault-auth \
      bound_service_account_names=app \
      bound_service_account_namespaces=default \
      policies=devops \
      ttl=24h

# jenkins
vault write \
      auth/kubernetes/role/jenkins \
      bound_service_account_names=vault-agent-injector \
      bound_service_account_names=vault-auth \
      bound_service_account_names=app \
      bound_service_account_namespaces=default \
      policies=jenkins \
      ttl=24h

# development
vault write \
      auth/kubernetes/role/development \
      bound_service_account_names=vault-agent-injector \
      bound_service_account_names=vault-auth \
      bound_service_account_names=app \
      bound_service_account_namespaces=default \
      policies=development \
      ttl=24h

# staging
vault write \
      auth/kubernetes/role/staging \
      bound_service_account_names=vault-agent-injector \
      bound_service_account_names=vault-auth \
      bound_service_account_names=app \
      bound_service_account_namespaces=default \
      policies=staging \
      ttl=24h

# production
vault write \
      auth/kubernetes/role/production \
      bound_service_account_names=vault-agent-injector \
      bound_service_account_names=vault-auth \
      bound_service_account_names=app \
      bound_service_account_namespaces=default \
      policies=production \
      ttl=24h


# Vault App Role Auth

# admin
vault write \
      auth/approle/role/admin \
      bound_service_account_names=vault-agent-injector \
      bound_service_account_names=vault-auth \
      bound_service_account_names=app \
      bound_service_account_namespaces=default \
      policies=admin \
      ttl=24h

# devops
vault write \
      auth/approle/role/devops \
      bound_service_account_names=vault-agent-injector \
      bound_service_account_names=vault-auth \
      bound_service_account_names=app \
      bound_service_account_namespaces=default \
      policies=devops \
      ttl=24h

# jenkins
vault write \
      auth/approle/role/jenkins \
      bound_service_account_names=vault-agent-injector \
      bound_service_account_names=vault-auth \
      bound_service_account_names=app \
      bound_service_account_namespaces=default \
      policies=jenkins \
      ttl=24h

# development
vault write \
      auth/approle/role/development \
      bound_service_account_names=vault-agent-injector \
      bound_service_account_names=vault-auth \
      bound_service_account_names=app \
      bound_service_account_namespaces=default \
      policies=development \
      ttl=24h

# staging
vault write \
      auth/approle/role/staging \
      bound_service_account_names=vault-agent-injector \
      bound_service_account_names=vault-auth \
      bound_service_account_names=app \
      bound_service_account_namespaces=default \
      policies=staging \
      ttl=24h

# production
vault write \
      auth/approle/role/production \
      bound_service_account_names=vault-agent-injector \
      bound_service_account_names=vault-auth \
      bound_service_account_names=app \
      bound_service_account_namespaces=default \
      policies=production \
      ttl=24h

