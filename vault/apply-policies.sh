#!/bin/bash


set -e


POLICY_NAMES=(admin devops development staging production jenkins default)


for policy_name in "${POLICY_NAMES[@]}"; do
  vault policy write $policy_name ./$policy_name.hcl
done
