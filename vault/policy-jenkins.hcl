# Mount the AppRole auth method
path "sys/auth/approle" {
  capabilities = [ "create", "read", "update", "delete", "sudo" ]
}

# Configure the AppRole auth method
path "sys/auth/approle/*" {
  capabilities = [ "create", "read", "update", "delete" ]
}

# Create and manage roles
path "auth/approle/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

# Write ACL policies
path "sys/policies/acl/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

path "sys/policy/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

path "secrets/development/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

path "secrets/staging/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

path "secrets/production/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

path "secrets/devops/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}
