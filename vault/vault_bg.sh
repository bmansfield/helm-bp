#!/bin/bash

PORT=8200

kubectl port-forward -n vault svc/vault $PORT &

while true ; do nc -vz 127.0.0.1 $PORT; sleep 10 ; done
