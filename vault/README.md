# VAULT

This is a highly available configuration with TLS on AWS. I would like to also
add a GCP version and add that in here as well.


## Features

Some of the features are:

* TLS
* HA
* Agent Injector
* Auto-Unlock via KMS
* Prometheus Node Exporter
* Raft


## What is included

Everything you should need aside from some secrets for the AWS keys. A
`Makefile` which automates away most of the tasks. All the scripts `make` uses
to generate the certificate and keys, AWS policy for KMS, vault policies,
scripts for initializing vault with everything you should need in order to just
start pushing secrets to it.


## Thing you will want to configure

The `genkeys.sh` certificate signing request configuration to match the company
name. Probably review the policies and roles. Make sure the defaults in here are
what you want. Maybe the service account for the application in the `apply-roles.sh`.
Also check the `VERSION` variable in the `Makefile` to adjust it to current vault
helm chart version.


## How to use


### Setup

Depending on what configuration and changes you are making this could change.

```bash
cp /path/to/this/repo/vault/* /my/new/project/vault/
cd /my/new/project/vault/
```

Make your changes to then configurations listed above to your liking

```bash
make init
make pull
make certs
make initkms
```

Replace the _helpers with one small change to the startup script which changes
vaults configuration file

```bash
cp _helpers.tpl templates/_helpers.tpl
```

### Deploy

It's finally time to launch it

```bash
make install
```

### Provisioning

Now you should have your HA Vault up and running in your kubernetes cluster in HA
mode with full TLS and Raft as the backend. This step is difficult to script and
automate, plus it should be handled with care, and you probably want to have greater
insight as to what is going on. Though it's probably the hardest and least desireable
step in the entire process. Fortunately you probably on need to do it just this once.

This step intentionally left out of automation for security reasons

```bash
kubectl exec -it vault-0 -- vault operator init
```

Save the output in a secure place. It should include the root token and the unseal
tokens.

Port forward the Pod

```bash
nohup ./vault_bg.sh &
```

Export the local vault address and login

```bash
export VAULT_ADDR=https://vault.default.svc:8200
vault login     # use the root token
make initvault
```

#### Warning!

There is an error in my scripts, until that gets fixed, you'll have to run this
manually. The bug is in the last step of `make initvault` which in the `apply-roles.sh`.
Specifically the first part where it tells vault how to authenticate with kubernetes.
So for the most part, you can still run it and it will do most of the things. In
the mean time, here is how to run that step manually so you can make sure it runs
correctly.

Be careful with this command. If you have multiple clusters in your kubernetes
config then it will pick the first one. You need to pick which one is the intended
cluster. Maybe try viewing all of them with `kubectl config view -o json | jq | less`

```bash
kubectl config view -o jsonpath="{.clusters[0].cluster.server}"
export CLUSTER_ADDR="output from kubectl config command"
export VAULT_SA_NAME=$(kubectl get sa vault -o jsonpath="{.secrets[*]['name']}")
export SA_JWT_TOKEN=$(kubectl get secret $VAULT_SA_NAME -o jsonpath="{.data.token}" | base64 --decode; echo)
export SA_CA_CRT=$(kubectl get secret $VAULT_SA_NAME -o jsonpath="{.data['ca\.crt']}" | base64 --decode; echo)
vault write auth/kubernetes/config token_reviewer_jwt="$SA_JWT_TOKEN" kubernetes_host="$CLUSTER_ADDR" kubernetes_ca_cert="$SA_CA_CRT"
```

Exit container and continue

Next exec into other vault pods and join them to leader you will need your vault-0
root token for this.

UPDATE: These next steps of joining the follows to the leader may no longer be
needed. Check with list-peers first before doing proceeding with this step. If
they are not there, then you may want to join them manually with the folling
instructions below.

```bash
kubectl exec -it vault-1 -- /bin/sh
VAULT_TOKEN=$ROOT_VAULT_TOKEN VAULT_API_ADDR=$VAULT_API_ADDR vault server -log-level=trace -config=/tmp/storageconfig.hcl > "/vault/logs/server.log" 2>&1 &
```

I seem to have issues on the next step if I do it too soon. It fails. Maybe it
takes some time before things start connecting and identifying services. Give it
a minute and try again if you have issues.

```bash
vault operator raft join -leader-ca-cert=@/var/run/secrets/kubernetes.io/serviceaccount/ca.crt -tls-skip-verify https://vault.default.svc:8200
vault login # use $ROOT_VAULT_TOKEN
vault operator raft list-peers
```

It should output the vault-0 pods as the leader, and the rest as the followers.
Exit the pod and repeat for the rest of the follower pods.

Confirm with the vault-0 that all the follower pods have joined successfully.

```bash
kubectl exec -it vault-0 -- /bin/sh
vault login # use $ROOT_VAULT_TOKEN
vault operator raft list-peers
```

### Add Secrets

For the most part you have vault successfully up and running and ready to use.
But there are no secrets in it yet.

Make sure you are still port forwarding and have your VAULT_ADDRESS exported, and
you are logged in as root, or any other role that is allowed to add the secrets.

You can add them manually or by file

```bash
vault kv put secrets/apps/$file @$file.json
```

### Helm

Now you should be ready to start using those secrets in your Helm Chart. Make sure
the service account the application is using is authorized to access the secrets
according to the approle service account and policy you specified in the
`apply-roles.sh` step.

Here is just an example of how you could add this to your helm chart's app `deployment.yaml`.
Please see the official vault documentation for all the various ways you could
inject these secrets into your deployment.

```bash
# deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "app.fullname" . }}
  labels:
    {{- include "app.labels" . | nindent 4 }}
    ...
spec:
  replicas: {{ .Values.puma.replicaCount }}
  selector:
    matchLabels:
      {{- include "app.selectorLabels" . | nindent 6 }}
{{- if .Values.strategy }}
  strategy:
    type: {{ .Values.strategy.type }}
{{- if eq quote .Values.strategy.type "RollingUpdate" }}
    rollingUpdate:
      maxSurge: {{ .Values.strategy.maxSurge }}
      maxUnavailable: {{ .Values.strategy.maxUnavailable }}
{{- end }}
{{- end }}
  template:
    metadata:
      labels:
        {{- include "app.selectorLabels" . | nindent 8 }}
      annotations:
        vault.hashicorp.com/agent-inject: "true"
        vault.hashicorp.com/agent-inject-status: "update"
        vault.hashicorp.com/tls-skip-verify: "true"
        vault.hashicorp.com/tls-secret: "vault-tls"
        vault.hashicorp.com/role: {{ .Values.serviceAccount.name }}
        vault.hashicorp.com/ca-cert: "/vault/tls/vault.ca"
        vault.hashicorp.com/agent-inject-secret-aws: "secrets/apps/foo"
        vault.hashicorp.com/agent-inject-template-foo: |
          {{`{{ with secret "secrets/apps/foo" }}
            {{ range $k, $v := .Data.data }}
              export {{ $k }}="{{ $v }}"
            {{ end }}
          {{ end }}`}}
    spec:
    {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      serviceAccountName: {{ .Values.serviceAccount.name }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          env:
          - name: VAULT_ADDR
            value: https://vault.default.svc:8200
          {{- with .Values.service.ports }}
          ports:
            {{- range . }}
            - name: {{.name}}
              containerPort: {{.port}}
            {{- end }}
          {{- end }}
          command: [ "/bin/bash", "-c", "source /vault/secrets/foo && ./app-start-cmd-goes-here.sh" ]
```
