path "secrets/production/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}

path "kv/production/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}
