#!/bin/bash

set -e


SERVICE=vault
NAMESPACE=default
SECRET_NAME=vault-tls
CSR_NAME=vault-csr
BASENAME=vault
TMPDIR=./cert


#
# Step 1: Create CSR
#


printf "\n\tCreating Vault Certificate Signing Request\n\r\n"

mkdir -p ${TMPDIR}

# Create private key
openssl genrsa -out ${TMPDIR}/${BASENAME}.key 2048

# Create a file ${TMPDIR}/${NAMESPACE}-csr.conf with the following contents
cat <<EOF >${TMPDIR}/${BASENAME}-csr.conf
[ req ]
default_bits = 2048
prompt = no
encrypt_key = yes
default_md = sha256
distinguished_name = dn
req_extensions = v3_req
[ dn ]
C = US
ST = New York
L = New York
O = ACME Co
emailAddress = devops@acme.com
CN = ${SERVICE}.${NAMESPACE}.svc
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names
[alt_names]
DNS.1 = ${SERVICE}
DNS.2 = ${SERVICE}.${NAMESPACE}
DNS.3 = ${SERVICE}.${NAMESPACE}.svc
DNS.4 = ${SERVICE}.${NAMESPACE}.svc.cluster.local
IP.1  = 127.0.0.1
EOF

# Create a CSR
openssl req \
  -new -key ${TMPDIR}/${BASENAME}.key \
  -config ${TMPDIR}/${BASENAME}-csr.conf \
  -subj "/CN=${SERVICE}.${NAMESPACE}.svc" \
  -days 365 \
  -out ${TMPDIR}/${BASENAME}.csr

printf "\n\tNew Vault Certificate Signing Request completed in ${TMPDIR}\n\r\n"


#
# Step 2: Create Certificate
#

printf "\n\tCreating Vault Certificate\n\r\n"

# Create a file ${TMPDIR}/${BASENAME}.yaml with the following contents
cat <<EOF >${TMPDIR}/${BASENAME}-csr.yaml
apiVersion: certificates.k8s.io/v1beta1
kind: CertificateSigningRequest
metadata:
  name: ${CSR_NAME}
spec:
  groups:
  - system:authenticated
  request: $(cat ${TMPDIR}/${BASENAME}.csr | base64 | tr -d '\n')
  usages:
  - digital signature
  - key encipherment
  - server auth
EOF

# Delete CSR and secret if exist
kubectl delete secret ${SECRET_NAME} --namespace ${NAMESPACE} --ignore-not-found
kubectl delete csr ${CSR_NAME} --namespace ${NAMESPACE} --ignore-not-found

# Send the CSR to Kubernetes.
kubectl create -f ${TMPDIR}/${BASENAME}-csr.yaml --namespace ${NAMESPACE}

# Approve the CSR in Kubernetes
kubectl certificate approve ${CSR_NAME} --namespace ${NAMESPACE}

printf "\n\tNew Vault Certificate approval complete\n\r\n"


#
# Step 3: Store key, cert and k8s CA into k8s secrets store
#

printf "\n\tPush key, cert, and CA to k8s secrets\n\r\n"

# Retrieve the certificate.
serverCert=$(kubectl get csr ${CSR_NAME} --template '{{.status.certificate}}')

# Write the certificate out to the CRT file
echo "${serverCert}" | openssl base64 -d -A -out ${TMPDIR}/${BASENAME}.crt

# Retrieve Kubernetes CA
# base64 this later for helm install in values injector.certs.caBundle override
# with --set flag
kubectl config \
        view \
        --raw \
        --minify \
        --flatten \
        -o jsonpath='{.clusters[].cluster.certificate-authority-data}' | \
        base64 -d > ${TMPDIR}/${BASENAME}.ca

# Store the key, cert, and Kubernetes CA into Kubernetes secrets.
### This will create files /vault/userconfig/vault.ca , /vault/userconfig/vault.crt, /vault/userconfig/vault.key in the vault Pods
kubectl create \
        secret \
        generic ${SECRET_NAME} \
        --namespace ${NAMESPACE} \
        --from-file=${BASENAME}.key=${TMPDIR}/${BASENAME}.key \
        --from-file=${BASENAME}.crt=${TMPDIR}/${BASENAME}.crt \
        --from-file=${BASENAME}.ca=${TMPDIR}/${BASENAME}.ca

# Verify the certificate:
openssl x509 -in ${TMPDIR}/${BASENAME}.crt -noout -text

printf "\n\tVault Kubernetes Secret is successfully stored and ready for use\n\r\n"

