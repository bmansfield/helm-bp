# GitLab Runner

[Official GitLab Runner Helm Chart](https://gitlab.com/gitlab-org/charts/gitlab-runner/-/tree/main)

Try to use latest stable version tags as oppose to master branch. Such as [https://gitlab.com/gitlab-org/charts/gitlab-runner/-/tree/v0.44.0/](https://gitlab.com/gitlab-org/charts/gitlab-runner/-/tree/v0.44.0/)

