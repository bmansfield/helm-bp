ENVIRONMENT ?= development
NAME ?= ingress-nginx
NAMESPACE ?= ingress-nginx
VALUES_FILE ?= values.yaml
PROJECT_VALUES_FILE ?=
INTERNAL_VALUES_FILE ?= internal-values.yaml
CHART_REPO ?= ingress-nginx
CHART_NAME ?= ingress-nginx
CHART ?= $(CHART_REPO)/$(CHART_NAME)
CHART_VERSION ?= 4.8.3

all: deploy

default: deploy

init repo:
	helm repo add $(NAME) https://kubernetes.github.io/ingress-nginx/
	helm repo update

delete del rm:
	helm delete $(NAME) -n $(NAMESPACE) || true

upgrade update deploy install:
ifeq ($(PROJECT_VALUES_FILE),)
	helm upgrade \
		-i $(NAME) \
		-n $(NAMESPACE) \
		-f $(VALUES_FILE) \
		--create-namespace \
		--version $(CHART_VERSION) \
		$(CHART)
else
	helm upgrade \
		-i $(NAME) \
		-n $(NAMESPACE) \
		-f $(VALUES_FILE) \
		-f $(PROJECT_VALUES_FILE) \
		--create-namespace \
		--version $(CHART_VERSION) \
		$(CHART)
endif

upgrade-internal update-internal deploy-internal install-internal:
	helm upgrade \
		-i $(NAME)-internal \
		-n $(NAMESPACE) \
		-f $(INTERNAL_VALUES_FILE) \
		--create-namespace \
		--version $(CHART_VERSION) \
		$(CHART)

ls list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'
.PHONY: init repo install upgrade update deploy delete del rm ls list
