# NGINX Ingress Controller for Kubernetes

[Ingress NGINX Controller](https://github.com/kubernetes/ingress-nginx)

ingress-nginx is an Ingress controller for Kubernetes using NGINX as a reverse proxy and load balancer.

Please read the [Official Documentation](https://kubernetes.github.io/ingress-nginx/)
for further information.


## About

You should only need to deploy this once per cluster. You can use this along with
other load balancer mechanisms as well. It is a Kubernetes Resource Type. You
can see them via `kubectl get ingressclass`. And define this in your `Ingress`
resource with defining the `ingressClassName` or Ingress Annotation of `kubernetes.io/ingress.class: nginx`.
If you do not specify either, the default will be selected for you.

I won't go into detail here, read the official documentation for details, but you
can select a few options of what type of load balancer, and on Ingress or Service,
and even TLS, or ACM, and much more.

An alternative to this could be Traefik.


## How To Deploy

More or less the same as the rest of the Helm Charts. Just use the `Makefile` or
run the native `helm upgrade --install ...` command. You should only need to
deploy it when you initially setup the cluster. And run the `make del` or `helm del`
command to take it down.

This one does not have nor need a `values.yaml` unless you care to make changes
to it's default configuration. Which may be necessary. I did not see any at first
glance.

