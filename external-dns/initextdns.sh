#!/bin/bash

#
# AWS IAM Policy Provisioning for External DNS
#


set -e


CLUSTER_NAME=${CLUSTER_NAME:=$1}
AWS_REGION=${AWS_REGION:=$2}
AWS_ACCOUNT=${AWS_ACCOUNT:=$3}
POLICY_ARN="arn:aws:iam::$AWS_ACCOUNT:policy/AllowExternalDNSUpdates"


eksctl create \
        iamserviceaccount \
        --name external-dns \
        --namespace kube-system \
        --role-name external-dns-$CLUSTER_NAME \
        --cluster $CLUSTER_NAME \
        --attach-policy-arn $POLICY_ARN \
        --override-existing-serviceaccounts \
        --approve

