# Exteranl DNS

[External DNS](https://github.com/kubernetes-sigs/external-dns)

[Official external-dns Helm Chart](https://github.com/kubernetes-sigs/external-dns/tree/master/charts/external-dns)

My version of external-dns helm chart in order to get me kick started for AWS.

You will need a hosted zone already created in order to work with this. I usually
manage them through Terraform, but doesn't have to be.


## IAM Role

```bash
make iam CLUSTER_NAME=dev AWS_ACCOUNT=123456789012
```


## Deploy

```bash
make deploy ENVIRONMENT_VALUES_FILE=dev-values.yaml
```

