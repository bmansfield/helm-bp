# Grafana Agent Operator

[Official Grafana Agent Helm Chart](https://github.com/grafana/helm-charts/tree/main/charts/agent-operator)

Container and Kubernetes log scraper compatible replacement for Promtail.


## How To Deploy

User the `Makefile` to deploy

```bash
make init
make deploy
```

Grafana Agent Operator is now running. But it's not collecting logs or metrics.
You will still need to do a few things first.


### Deploy CRDs

The Grafana Agent Operator has 3 main CRD's (Custom Resource Definitions);
`GrafanaAgent`, `MetricsInstance`, and `LogsInstance`. We need to create these
based on your project. If it's an existing project and you just want to update
the helm chart, you shouldn't need to redeploy these. If it's a new project,
then you probably want to copy and existing file of these three CRD's, modify
the relative labels and external labels. Then you should be ready to deploy.

```bash
make setup CLUSTER=<cluster-name>
```

Now it's ready to use. But you still need `ServiceMonitor`'s and `PodLogs`
deployed by your individual application. These live in the `kube-state-metrics`
helm chart. Please read the `kube-state-metrics` `README.md` on instructions of
how to apply those. Once those are properly labeled in a way so that these CRD's
can pick up on them. Then it should start sending metrics and logs.

Please continue reading the next segment. Because you are likely not done yet.


### Kubernetes Observability

This is a new feature in Grafana. And it will collect even more metrics. You
will want to deploy the `kube-state-metrics` helm chart next. Please navigate
to that helm chart and follow the `README.md` instructions there.


