# ArgoCD

[ArgoCD Docs](https://argo-cd.readthedocs.io/en/stable/)
[ArgoCD Community Helm Chart](https://github.com/argoproj/argo-helm/tree/main/charts/argo-cd)

A GitOps continuous delivery tool for Kubernetes.


# Deployment

```bash
make deploy ENVIRONMENT_VALUES_FILE=dev-values.yaml
```


# Setup

Now that argo is deployed. We need to set it up. Each of these steps you will
need to copy the existing example files, and create project/application specific
versions of these before you run the make commands.


## Accounts

First lets create some accounts to work with.

```bash
make accounts
```


## Application Specific

You're application will need both an `Application` and a `Project. Both ArgoCD
CRD's that get installed with deployment of argo.

```bash
make apps
```


