#!/bin/bash

#
# Creates the iam service account to manage other clusters
#


set -e


CLUSTER=$1


# Create the IAM Policy
POLICY_ARN=$(aws iam \
                  create-policy \
                  --policy-name argocd-$CLUSTER \
                  --policy-document file://cluster-policy.json | \
                  jq -r '.Policy.Arn')


# Create the IAM Role with OIDC connection
aws iam \
    create-role \
    --role-name argocd-$CLUSTER \
    --assume-role-policy-document file://trust-policy.json


# Attach the IAM policy to the roll
aws iam \
    attach-role-policy \
    --role-name argocd-$CLUSTER \
    --policy-arn $POLICY_ARN

